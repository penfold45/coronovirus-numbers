FROM node:12-alpine

RUN apk update
RUN npm install -g serverless 

WORKDIR /var/www/translation

COPY package*.json ./
COPY . .

EXPOSE 3000