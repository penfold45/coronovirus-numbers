'use strict';
import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import axios from 'axios';
var plotly = require('plotly')("penfold45", "bCgmh3P3fBlG6zvCocDI");

function getSite() {
  return {
      id: 1,
      name: "IAM_A_SITE"
  }
}

async function getUKData(area: any) {
  console.error('IAM HERE');
  const response = await axios.get('https://coronavirus.data.gov.uk/downloads/json/coronavirus-cases_latest.json')
  return response.data[area];
}

async function getData(area:any, areaName:any) {
  const ukData = await getUKData(area);
  console.log(areaName);
  return ukData.filter(
    obj => {
      console.log(obj.areaName)
      return obj.areaName === areaName
    });
}

async function getPlottingData(area, areaName) {
  const londonData = await getData(area, areaName); 

  var xAxis = [];
  var yAxis = [];
  var i = 0;
  var j = 0;

  for (j = 0; j< londonData.length; j++) {
    xAxis[i] = londonData[j].specimenDate;
    yAxis[i] = londonData[j].dailyLabConfirmedCases;
    i++;
  }

  return {
    x: xAxis,
    y: yAxis,
    type: "scatter"
  };

}

export const listAll: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent, context: Context) : Promise<APIGatewayProxyResult> => {
  var layout = {fileopt : "overwrite", filename : "london-corona"};
  plotly.plot(await getPlottingData('regions', 'London'), layout, function (err, msg) {
    if (err) return console.log(err);
    console.log(msg);
  });

  var layout = {fileopt : "overwrite", filename : "rugby-corona"};
  plotly.plot(await getPlottingData('ltlas', 'Rugby'), layout, function (err, msg) {
    if (err) return console.log(err);
    console.log(msg);
  });
  return {
    statusCode: 200,
    body: JSON.stringify(
      await getPlottingData('regions', 'London')
      ,
      null,
      2
    ),
  };
};

